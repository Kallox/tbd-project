from typing import Optional

from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.taxon.models import Image

init_models()

ImageSchema = pydantic_model_creator(
    Image, name="Image", exclude=("records", "core_taxons")
)
ImageSummarySchema = pydantic_model_creator(
    Image,
    name="ImageSummary",
    exclude=("image", "category", "core_taxons", "records"),
)

#:
class ImageCreateSchema(BaseModel):
    image: str
    category: str
    description: str
    author: str
    source: str

    class Config:
        title = "Image Create"
