from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.taxon.models import Order
from app.modules.taxon.schemas.taxonomic_class import ClassCreateSchema

init_models()

OrderSchema = pydantic_model_creator(Order, name="Order")


#:
class OrderCreateSchema(BaseModel):
    name: str
    taxonomic_class: ClassCreateSchema

    class Config:
        title = "Order Create"
