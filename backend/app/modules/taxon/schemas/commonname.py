from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.taxon.models import CommonName

init_models()

CommonNameSchema = pydantic_model_creator(CommonName, name="CommonName")

#.
class CommonNameCreateSchema(BaseModel):
    name: str

    class Config:
        title = "CommonName Create"