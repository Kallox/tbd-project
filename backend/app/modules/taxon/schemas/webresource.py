from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.taxon.models import WebResource

init_models()

WebResourceSchema = pydantic_model_creator(WebResource, name="WebResource")

#:
class WebResourceCreateSchema(BaseModel):
    name: str
    url: str

    class Config:
        title = "WebResource Create"