from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.taxon.models import Family
from app.modules.taxon.schemas.order import OrderCreateSchema

init_models()

FamilySchema = pydantic_model_creator(Family, name="Family")

#.
class FamilyCreateSchema(BaseModel):
    name: str
    order: OrderCreateSchema

    class Config:
        title = "Family Create"
