from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.taxon.models import Species
from app.modules.taxon.schemas.genus import GenusCreateSchema

init_models()

SpeciesSchema = pydantic_model_creator(Species, name="Species")


#:
class SpeciesCreateSchema(BaseModel):
    name: str
    genus: GenusCreateSchema

    class Config:
        title = "Species Create"
