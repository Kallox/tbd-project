from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.taxon.models import Genus
from app.modules.taxon.schemas.family import FamilyCreateSchema

init_models()

GenusSchema = pydantic_model_creator(Genus, name="Genus")

#:
class GenusCreateSchema(BaseModel):
    name: str
    family: FamilyCreateSchema

    class Config:
        title = "Genus Create"
