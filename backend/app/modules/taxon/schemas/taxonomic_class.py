from typing import List

from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.taxon.models import Class
from app.modules.taxon.schemas.phylum import PhylumCreateSchema

init_models()

ClassSchema = pydantic_model_creator(
    Class, name="Class", exclude=("phylum", "orders", "id", "phylum_id")
)


class ClassSummarySchema(BaseModel):
    taxonomic_class: ClassSchema
    quantity: int

    class Config:
        title = "ClassGet"


#:
class ClassCreateSchema(BaseModel):
    name: str
    phylum: PhylumCreateSchema

    class Config:
        title = "Class Create"
