from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.taxon.models import SubSpecies
from app.modules.taxon.schemas.species import SpeciesCreateSchema

init_models()
#:
SubSpeciesSchema = pydantic_model_creator(SubSpecies, name="SubSpecies")


# Unused
class SubSpeciesCreateSchema(BaseModel):
    name: str
    species: SpeciesCreateSchema

    class Config:
        title = "Subspecies Create"
