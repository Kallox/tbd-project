from typing import List, Optional

from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.schemas.author import AuthorCreateSchema
from app.modules.taxon.models import Taxon
from app.modules.taxon.schemas.commonclassification import CommonClassificationSchema
from app.modules.taxon.schemas.commonname import CommonNameCreateSchema
from app.modules.taxon.schemas.species import SpeciesCreateSchema
from app.modules.taxon.schemas.synonym import SynonymCreateSchema
from app.modules.taxon.schemas.webresource import WebResourceCreateSchema

init_models()

TaxonSchema = pydantic_model_creator(
    Taxon,
    name="Taxon",
    exclude=(
        "references.records",
        "species.subspecies",
        "subspecies.species",
        "image.records",
        "author.references",
        "records",
    )
)
TaxonAbstractSchema = pydantic_model_creator(
    Taxon,
    name="TaxonAbstract",
    exclude=(
        "condensed_name",
        "author",
        "common_classification",
        "species.genus",
        "species.subspecies",
        "subspecies.species",
        "image.records",
        "records",
        "web_resources",
        "synonyms",
        "common_names",
        "author_id",
        "common_classification_id"
    )
)
#Hay un problema... no puedo obtener de las tablas order en adelante...
TaxonGroupSchema = pydantic_model_creator(
    Taxon,
    name="TaxonGroup",
    exclude=(
        "species.subspecies",
        "subspecies.species",
        "author.references",
        "image.records",
        "records",
        "web_resources",
    )
)


class TaxonPaginateSchema(BaseModel):
    taxon: List[TaxonGroupSchema]
    page_limit: int

    class Config:
        title = "Paginate Taxon"

class TaxonCreateSchema(BaseModel):
    scientific_name: str
    condensed_name: str
    author: Optional[AuthorCreateSchema] = None
    common_classification: Optional[CommonClassificationSchema] = None
    species: SpeciesCreateSchema
    subspecies: Optional[str] = None
    webresource: List[WebResourceCreateSchema] = []
    synonym: List[SynonymCreateSchema] = []
    commonname: List[CommonNameCreateSchema] = []

    class Config:
        title = "Taxon Create"
