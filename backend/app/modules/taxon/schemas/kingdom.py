from typing import List

from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.taxon.models import Kingdom

init_models()

KingdomSchema = pydantic_model_creator(Kingdom, name="Kingdom", exclude=("phyla",))
KingdomPlainSchema = pydantic_model_creator(
    Kingdom,
    name="KingdomPlain",
    exclude=(
        "phyla",
        "id",
    ),
)


class KingdomCreateSchema(BaseModel):
    name: str

    class Config:
        title = "Kingdom Create"

#:
class KingdomSummarySchema(BaseModel):
    kingdom: KingdomPlainSchema
    quantity: int

    class Config:
        title = "Kingdom Get"
