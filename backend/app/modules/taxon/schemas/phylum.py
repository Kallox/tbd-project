from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.taxon.models import Phylum
from app.modules.taxon.schemas.kingdom import KingdomCreateSchema

init_models()

PhylumSchema = pydantic_model_creator(Phylum, name="Phylum")

#:
class PhylumCreateSchema(BaseModel):
    name: str
    kingdom: KingdomCreateSchema

    class Config:
        title = "Phylum Create"
