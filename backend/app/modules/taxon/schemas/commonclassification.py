from typing import List

from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.taxon.models import CommonClassification

init_models()

CategorySchema = pydantic_model_creator(
    CommonClassification, name="CommonClassification", exclude=("taxons", "id")
)


class CategorySummarySchema(BaseModel):
    category: CategorySchema
    quantity: int

    class Config:
        title = "Category Get"


#Debido a que no tiene que ver con la API directamente, mantendre el nombre original.
class CommonClassificationSchema(BaseModel):
    name: str
    short_name: str

    class Config:
        title = "CommonClassification Create"