from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.taxon.models import Synonym

init_models()

SynonymSchema = pydantic_model_creator(Synonym, name="Synonym")

#:
class SynonymCreateSchema(BaseModel):
    name: str

    class Config:
        title = "Synonym Create"