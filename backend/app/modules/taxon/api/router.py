from fastapi import APIRouter

from app.modules.taxon.api.endpoints import (
    commonclassification,
    image,
    kingdom,
    taxon,
    taxonomic_class,
)

api_router = APIRouter()

# .

api_router.include_router(kingdom.router, prefix="/kingdom", tags=["kingdom"])
api_router.include_router(taxon.router, prefix="/taxon", tags=["taxon"])
api_router.include_router(taxonomic_class.router, prefix="/class", tags=["class"])
api_router.include_router(
    commonclassification.router, prefix="/categories", tags=["categories"]
)
api_router.include_router(image.router, prefix="/image", tags=["image"])
