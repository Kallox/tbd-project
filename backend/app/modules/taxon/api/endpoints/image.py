import sys
from typing import Optional, List

from fastapi import APIRouter, HTTPException

from app.modules.occurrence.models import OccurrenceRecord
from app.modules.taxon.schemas.image import ImageSchema, ImageCreateSchema

from app.modules.taxon.models import Image, Taxon

router = APIRouter()


@router.get("/taxon/{taxon_id}", response_model=List[ImageSchema])
async def read_taxon_image(taxon_id: int) -> List[ImageSchema]:
    taxon_db = await Taxon.get(id=taxon_id)
    return await ImageSchema.from_queryset(taxon_db.image.all())


@router.get("/ocurrence/{ocurrence_id}", response_model=List[ImageSchema])
async def read_occurrence_image(ocurrence_id: int) -> List[ImageSchema]:
    occurrence_db = await OccurrenceRecord.get(id=ocurrence_id)
    return await ImageSchema.from_queryset(occurrence_db.image.all())


@router.post("/taxon", response_model=ImageSchema)
async def create_taxon_image(
        id: int,
        image: ImageCreateSchema,
) -> ImageSchema:
    taxon_db = await Taxon.get(id=id)
    image_db = await Image.create(**image.dict(
        exclude_unset=True,
        exclude={
           "id_taxon",
           "id_occurrence",
       }
    ))
    await taxon_db.image.add(image_db)
    return await ImageSchema.from_queryset_single(Image.get(id=image_db.id))


@router.post("/occurrence", response_model=ImageSchema)
async def create_occurrence_image(
        id: int,
        image: ImageCreateSchema,
) -> ImageSchema:
    occurrence_db = await OccurrenceRecord.get(id=id)
    image_db = await Image.create(**image.dict(
        exclude_unset=True,
        exclude={
            "id_taxon",
            "id_occurrence",
        }
    ))
    await occurrence_db.image.add(image_db)
    return await ImageSchema.from_queryset_single(Image.get(id=image_db.id))


@router.delete("/{image_id}")
async def delete_image(image_id: int) -> str:
    try:
        await Image.filter(id=image_id).delete()
    except:
        return sys.exc_info()[0]
    return True