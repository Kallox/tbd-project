from typing import List

from fastapi import APIRouter

from app.modules.taxon.models import CommonClassification as Category, Taxon
from app.modules.taxon.schemas.commonclassification import CategorySummarySchema

router = APIRouter()


@router.get("/", response_model=List[CategorySummarySchema])
async def read_categories() -> List[CategorySummarySchema]:
    category_db = await Category.all()
    taxon_db = [await Taxon.filter(
        common_classification__name=c.name
    ) for c in category_db]
    quantity = [len(t) for t in taxon_db]
    return [CategorySummarySchema.construct(
        category=c, quantity=quantity[i]) for i, c in enumerate(category_db, start=0)]

