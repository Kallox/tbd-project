from typing import List

from fastapi import APIRouter

from app.modules.taxon.models import Class, Taxon
from app.modules.taxon.schemas.taxonomic_class import ClassSummarySchema

router = APIRouter()


@router.get("/", response_model=List[ClassSummarySchema])
async def read_classes() -> List[ClassSummarySchema]:
    class_db = await Class.all()
    taxon_db = [await Taxon.filter(
        species__genus__family__order__class_field__name=c.name
    ) for c in class_db]
    quantity = [len(t) for t in taxon_db]
    return [ClassSummarySchema.construct(
        taxonomic_class=c, quantity=quantity[i]) for i, c in enumerate(class_db, start=0)]


