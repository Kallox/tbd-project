import sys
from random import randint
from typing import Optional, List

from fastapi import APIRouter, Query
from tortoise.contrib.fastapi import HTTPNotFoundError

from app.modules.taxon.models import Taxon, CommonName, CommonClassification, Species, SubSpecies, Genus, Kingdom, \
    Phylum, Class, Order, Family, WebResource, Synonym
from app.modules.occurrence.models import Author
from app.modules.taxon.schemas.taxon import TaxonSchema, TaxonCreateSchema, TaxonAbstractSchema, TaxonPaginateSchema, \
    TaxonGroupSchema

router = APIRouter()


@router.get("/count", response_model=int)
async def count_taxon() -> int:
    return await Taxon.all().count()


@router.get("/random", response_model=TaxonAbstractSchema)
async def show_random_taxon() -> TaxonAbstractSchema:
    taxon_db = await Taxon.all()
    index = randint(0, len(taxon_db)-1)
    return await TaxonAbstractSchema.from_queryset_single(Taxon.get(id=taxon_db[index].id))


@router.get("/paginate", response_model=TaxonPaginateSchema)
async def paginate_taxon(
        page: int = Query(default=1, ge=1),
        limit: int = Query(default=42),
        category: Optional[str] = None,
        kingdom: Optional[str] = None,
        taxonomic_class: Optional[str] = None,
        query: Optional[str] = None
) -> TaxonPaginateSchema:
    query_db = {}
    if category:
        query_db.update({"common_classification__name": category})
    if kingdom:
        query_db.update({"species__genus__family__order__class_field__phylum__kingdom__name": kingdom})
    if taxonomic_class:
        query_db.update({"species__genus__family__order__class_field__name": taxonomic_class})
    if query:
        query_db.update({"scientific_name": query})

    taxon_db = Taxon.filter(**query_db).offset((page-1)*limit).limit(limit)
    page_limit = len(await Taxon.filter(**query_db))/limit

    return TaxonPaginateSchema.construct(
        taxon=await TaxonGroupSchema.from_queryset(taxon_db),
        page_limit=page_limit
    )


@router.get(
    "/{taxon_id}",
    response_model=TaxonSchema,
    responses={404: {"model": HTTPNotFoundError}},
)
async def read_taxon(taxon_id: int) -> TaxonSchema:
    return await TaxonSchema.from_queryset_single(Taxon.get(id=taxon_id))


@router.post("/", response_model=TaxonSchema)
async def create_taxon(taxon: TaxonCreateSchema) -> TaxonSchema:
    #Muchos de estos deben ser solo get...
    kingdom = await Kingdom.get_or_create(
        name=taxon.species.genus.family.order.taxonomic_class.phylum.kingdom.name
    )
    phylum = await Phylum.get_or_create(
        name=taxon.species.genus.family.order.taxonomic_class.phylum.name,
        kingdom_id=kingdom[0].id
    )
    taxonomic_class = await Class.get_or_create(
        name=taxon.species.genus.family.order.taxonomic_class.name,
        phylum_id=phylum[0].id
    )
    order = await Order.get_or_create(
        name=taxon.species.genus.family.order.name,
        class_field_id=taxonomic_class[0].id
    )
    family = await Family.get_or_create(
        name=taxon.species.genus.family.name,
        order_id=order[0].id
    )
    genus = await Genus.get_or_create(
        name=taxon.species.genus.name,
        family_id=family[0].id
    )
    species = await Species.get_or_create(
        name=taxon.species.name,
        genus_id=genus[0].id
    )
    subspecies = await SubSpecies.get_or_create(
        name=taxon.subspecies,
        species_id=species[0].id
    )
    author = await Author.get_or_create(
        name=taxon.author.name
    )
    common_classification = await CommonClassification.get_or_create(
        name=taxon.common_classification.name,
        short_name=taxon.common_classification.short_name
    )
    taxon_db = await Taxon.create(**taxon.dict(
            exclude_unset=True,
            exclude={
                "author",
                "common_classification",
                "species",
                "subspecies",
                "webresource",
                "synonym",
                "commonname"
            }),
            author_id=author[0].id,
            common_classification_id=common_classification[0].id,
            species_id=species[0].id,
            subspecies_id=subspecies[0].id
            )
    #Variables no usadas...
    webresource = [await WebResource.create(
        name=w.name,
        url=w.url,
        taxon_id=taxon_db.id
    ) for w in taxon.webresource]
    synonym = [await Synonym.create(
        name=s.name,
        taxon_id=taxon_db.id
    ) for s in taxon.synonym]
    commonname = [await CommonName.create(
        name=c.name,
        taxon_id=taxon_db.id
    ) for c in taxon.commonname]
    return await TaxonSchema.from_queryset_single(Taxon.get(id=taxon_db.id))


@router.delete("/{taxon_id}")
async def delete_taxon(taxon_id: int) -> str:
    try:
        await Taxon.filter(id=taxon_id).delete()
    except:
        return sys.exc_info()[0]
    return True

