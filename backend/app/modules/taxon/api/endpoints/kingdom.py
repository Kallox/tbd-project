from random import randint
from typing import List

from fastapi import APIRouter

from app.modules.taxon.schemas.kingdom import KingdomSchema, KingdomPlainSchema, KingdomCreateSchema, KingdomSummarySchema

from app.modules.taxon.models import Kingdom, Taxon

router = APIRouter()

# TODO eliminar.
@router.get("/{kingdom_id}", response_model=KingdomPlainSchema)
async def read_kingdom(kingdom_id: int) -> KingdomPlainSchema:
    return await KingdomPlainSchema.from_queryset_single(Kingdom.get(id=kingdom_id))


# TODO eliminar.
@router.post("/", response_model=KingdomSchema)
async def create_kingdom(kingdom: KingdomCreateSchema) -> KingdomSchema:
    kingdom_dict = kingdom.dict(exclude_unset=True)
    kingdom_db = await Kingdom.create(**kingdom_dict)

    return await KingdomSchema.from_tortoise_orm(kingdom_db)


@router.get("/", response_model=List[KingdomSummarySchema])
async def read_kingdoms() -> List[KingdomSummarySchema]:
    kingdom_db = await Kingdom.all()
    taxon_db = [await Taxon.filter(
        species__genus__family__order__class_field__phylum__kingdom__name=k.name
    ) for k in kingdom_db]
    quantity = [len(t) for t in taxon_db]
    return [KingdomSummarySchema.construct(kingdom=k, quantity=quantity[i]) for i, k in enumerate(kingdom_db, start=0)]
