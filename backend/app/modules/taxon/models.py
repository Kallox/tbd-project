from typing import TYPE_CHECKING

from tortoise import Model, fields

if TYPE_CHECKING:
    from app.modules.occurrence.models import OccurrenceRecord


class Kingdom(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=32, null=False, unique=True)

    phyla: fields.ReverseRelation["Phylum"]

    class Meta:
        table = "core_kingdom"
        ordering = ("name",)


class Phylum(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=32, null=False, unique=True)
    kingdom = fields.ForeignKeyField(
        "models.Kingdom",
        null=False,
        on_delete="RESTRICT",
        related_name="phyla",
    )

    classes: fields.ReverseRelation["Class"]

    class Meta:
        table = "core_phylum"
        ordering = ("name",)


class Class(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=32, null=False, unique=True)
    phylum = fields.ForeignKeyField(
        "models.Phylum", null=False, on_delete="RESTRICT", related_name="classes"
    )

    orders: fields.ReverseRelation["Order"]

    class Meta:
        table = "core_class"
        ordering = ("name",)


class Order(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=32, null=False, unique=True)
    class_field = fields.ForeignKeyField(
        "models.Class", null=False, on_delete="RESTRICT", related_name="orders"
    )

    families: fields.ReverseRelation["Family"]

    class Meta:
        table = "core_order"
        ordering = ("name",)


class Family(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=32, null=False)
    order = fields.ForeignKeyField(
        "models.Order", null=False, on_delete="RESTRICT", related_name="families"
    )

    genera: fields.ReverseRelation["Genus"]

    class Meta:
        table = "core_family"
        ordering = ("name",)


class Genus(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=32, null=False, unique=True)
    family = fields.ForeignKeyField(
        "models.Family", null=False, on_delete="RESTRICT", related_name="genera"
    )

    species: fields.ReverseRelation["Species"]

    class Meta:
        table = "core_genus"
        ordering = ("name",)


class Species(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=64, null=False, unique=True)
    genus = fields.ForeignKeyField(
        "models.Genus",
        null=False,
        on_delete="RESTRICT",
        related_name="species",
    )

    subspecies: fields.ReverseRelation["Species"]
    taxons: fields.ReverseRelation["Taxon"]

    class Meta:
        table = "core_species"
        ordering = ("name",)


class SubSpecies(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=32, null=False)
    species = fields.ForeignKeyField(
        "models.Species",
        null=False,
        on_delete="RESTRICT",
        related_name="subspecies",
    )

    taxons: fields.ReverseRelation["Taxon"]

    class Meta:
        table = "core_subspecies"
        ordering = ("name",)


class CommonClassification(Model):
    id = fields.IntField(pk=True)
    short_name = fields.CharField(max_length=3, null=False, index=True, unique=True)
    name = fields.CharField(max_length=32, null=False)

    taxons: fields.ReverseRelation["Taxon"]

    class Meta:
        table = "core_commonclassification"
        ordering = ("name",)


class Taxon(Model):
    id = fields.IntField(pk=True)
    scientific_name = fields.CharField(max_length=64, null=False)
    condensed_name = fields.CharField(max_length=64, null=False, unique=True)
    author = fields.ForeignKeyField(
        "models.Author", related_name="taxons", on_delete="SET NULL", null=True
    )
    common_classification = fields.ForeignKeyField(
        "models.CommonClassification",
        null=True,
        on_delete="RESTRICT",
        related_name="taxons",
    )
    species = fields.ForeignKeyField(
        "models.Species",
        null=False,
        on_delete="RESTRICT",
        related_name="taxons",
    )
    subspecies = fields.ForeignKeyField(
        "models.SubSpecies", null=True, on_delete="RESTRICT", related_name="taxons"
    )
    image = fields.ManyToManyField(
        "models.Image",
        through="core_taxon_images",
        null=False,
        on_delete="CASCADE",
    )

    records: fields.ReverseRelation["OccurrenceRecord"]
    web_resources: fields.ReverseRelation["WebResource"]
    synonyms: fields.ReverseRelation["Synonym"]
    common_names: fields.ReverseRelation["CommonName"]

    class Meta:
        table = "core_taxon"
        ordering = ("scientific_name",)


class Image(Model):
    id = fields.IntField(pk=True)
    image = fields.CharField(max_length=100, null=False)
    category = fields.CharField(max_length=64, null=False)
    description = fields.CharField(max_length=255, null=False)
    author = fields.CharField(max_length=64, null=False)
    source = fields.CharField(max_length=512, null=False)
    reputation = fields.SmallIntField(default=20, null=False)

    records: fields.ReverseRelation["OccurrenceRecord"]

    class Meta:
        table = "core_image"
        ordering = (
            "-reputation",
            "id",
        )


class WebResource(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=64, null=False)
    url = fields.CharField(max_length=200, null=False)
    taxon = fields.ForeignKeyField(
        "models.Taxon",
        null=False,
        on_delete="CASCADE",
        related_name="web_resources",
    )

    class Meta:
        table = "core_webresource"
        ordering = (
            "taxon_id",
            "name",
        )


class Synonym(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=128, null=False)
    taxon = fields.ForeignKeyField(
        "models.Taxon", null=False, on_delete="CASCADE", related_name="synonyms"
    )

    class Meta:
        table = "core_synonym"
        ordering = ("name",)


class CommonName(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=64, null=False)
    taxon = fields.ForeignKeyField(
        "models.Taxon", null=False, on_delete="CASCADE", related_name="common_names"
    )

    class Meta:
        table = "core_commonname"
        ordering = ("name",)
