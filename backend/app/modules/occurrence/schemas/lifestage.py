from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.models import LifeStage

init_models()

LifeStageSchema = pydantic_model_creator(LifeStage, name="LifeStage")


class LifeStageCreateSchema(BaseModel):
    shortname: str
    name: str

    class Config:
        title = "Life Stage Create"
