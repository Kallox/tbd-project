from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.models import Journal

init_models()

JournalSchema = pydantic_model_creator(Journal, name="Journal")


class JournalCreateSchema(BaseModel):
    name: str
    country: str

    class Config:
        title = "Journal Create"
