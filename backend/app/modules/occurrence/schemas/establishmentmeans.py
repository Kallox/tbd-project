from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.models import Establishmentmeans

init_models()

EstablishmentmeansSchema = pydantic_model_creator(
    Establishmentmeans, name="Establishmentmeans"
)


class EstablishmentmeansCreateSchema(BaseModel):
    name: str

    class Config:
        title = "Establishment Create"
