from typing import Optional

from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.models import OccurrenceRecord
from app.modules.occurrence.schemas.dataprovider import DataProviderCreateSchema
from app.modules.occurrence.schemas.establishmentmeans import (
    EstablishmentmeansCreateSchema,
)
from app.modules.occurrence.schemas.lifestage import LifeStageCreateSchema
from app.modules.occurrence.schemas.location import LocationCreateSchema
from app.modules.occurrence.schemas.recordtype import RecordTypeCreateSchema
from app.modules.occurrence.schemas.reference import ReferenceCreateSchema
from app.modules.occurrence.schemas.substratumtype import SubstratumTypeCreateSchema

init_models()

OccurrenceRecordSchema = pydantic_model_creator(
    OccurrenceRecord, name="OccurrenceRecord"
)

OccurrenceRecordPlainSchema = pydantic_model_creator(
    OccurrenceRecord,
    name="OccurrenceRecordPlain",
    exclude=("taxon", "image", "reference.authors.taxons"),
)


class OccurrenceRecordCreateSchema(BaseModel):
    taxon: str
    quantity: Optional[int] = None
    date: Optional[str] = None
    latitude: Optional[float] = None
    longitude: Optional[float] = None
    min_altitude: Optional[float] = None
    max_altitude: Optional[float] = None
    comments: Optional[str] = None
    data_provider: Optional[DataProviderCreateSchema]
    establishment: Optional[EstablishmentmeansCreateSchema]
    life_stage: Optional[LifeStageCreateSchema]
    location: Optional[LocationCreateSchema]
    record_type: str
    reference: Optional[ReferenceCreateSchema]
    substratum_type: Optional[SubstratumTypeCreateSchema]

    class Config:
        title = "Occurrence Record Create"
