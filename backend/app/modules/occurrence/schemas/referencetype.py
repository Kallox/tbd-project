from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.models import ReferenceType

init_models()

ReferenceTypeSchema = pydantic_model_creator(ReferenceType, name="ReferenceType")


class ReferenceTypeCreateSchema(BaseModel):
    name: str

    class Config:
        title = "Reference Type Create"
