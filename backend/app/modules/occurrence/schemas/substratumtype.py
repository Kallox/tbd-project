from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.models import SubstratumType

init_models()

SubstratumTypeSchema = pydantic_model_creator(SubstratumType, name="SubstratumType")


class SubstratumTypeCreateSchema(BaseModel):
    name: str

    class Config:
        title = "Substratum Type Create"
