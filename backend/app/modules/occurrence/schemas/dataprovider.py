from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.models import DataProvider

init_models()

DataProviderSchema = pydantic_model_creator(DataProvider, name="DataProvider")


class DataProviderCreateSchema(BaseModel):
    name: str
    address: str
    city: str
    email: str
    telephone: str

    class Config:
        title = "Data Provider Create"
