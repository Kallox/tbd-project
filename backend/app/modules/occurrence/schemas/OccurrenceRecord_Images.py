from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.models import OccurrenceRecord

init_models()
