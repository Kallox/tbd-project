from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.models import Author

init_models()

AuthorSchema = pydantic_model_creator(Author, name="Author")


class AuthorCreateSchema(BaseModel):
    name: str

    class Config:
        title = "Author Create"
