from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.models import RecordType

init_models()

RecordTypeSchema = pydantic_model_creator(RecordType, name="RecordType")


class RecordTypeCreateSchema(BaseModel):
    name: str

    class Config:
        title = "Record Type Create"
