from typing import List, Optional

from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.models import Reference
from app.modules.occurrence.schemas.author import AuthorCreateSchema
from app.modules.occurrence.schemas.journal import JournalCreateSchema
from app.modules.occurrence.schemas.referencetype import ReferenceTypeCreateSchema

init_models()
ReferenceSchema = pydantic_model_creator(
    Reference, name="Reference", exclude=("journal", "reference_type", "authors")
)


class ReferenceCreateSchema(BaseModel):
    name: str
    cite: str
    doi: str
    url: str
    journal: Optional[JournalCreateSchema]
    reference_type: Optional[ReferenceTypeCreateSchema]
    authors: Optional[List[AuthorCreateSchema]]

    class Config:
        title = "Reference Create"
