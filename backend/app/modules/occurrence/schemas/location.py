from typing import Optional

from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.models import Location
from app.modules.occurrence.schemas.administrativedivision import (
    AdministrativeDivisionCreateSchema,
)

init_models()

LocationSchema = pydantic_model_creator(Location, name="Location")


class LocationCreateSchema(BaseModel):
    locality: str
    administrative_division: Optional[AdministrativeDivisionCreateSchema] = None

    class Config:
        title = "Location Create"
