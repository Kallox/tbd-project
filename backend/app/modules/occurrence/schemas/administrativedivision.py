from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator

from app.db.base import init_models
from app.modules.occurrence.models import AdministrativeDivision

init_models()

AdministrativeDivisionSchema = pydantic_model_creator(
    AdministrativeDivision, name="AdministrativeDivision"
)


class AdministrativeDivisionCreateSchema(BaseModel):
    country_code: str
    state_province: str
    country: str
    municipality: str

    class Config:
        title = "Administrative Division Create"
