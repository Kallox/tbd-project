from typing import TYPE_CHECKING
from uuid import uuid4

from tortoise import Model, fields

if TYPE_CHECKING:
    from app.modules.taxon.models import Taxon


class SubstratumType(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=255)

    records: fields.ReverseRelation["OccurrenceRecord"]

    class Meta:
        table = "core_substratumtype"


class DataProvider(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=64)
    address = fields.CharField(max_length=64)
    city = fields.CharField(max_length=32)
    email = fields.CharField(max_length=32)
    telephone = fields.CharField(max_length=16)

    records: fields.ReverseRelation["OccurrenceRecord"]

    class Meta:
        table = "core_dataprovider"


class RecordType(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=32)

    records: fields.ReverseRelation["OccurrenceRecord"]

    class Meta:
        table = "core_recordtype"


class Establishmentmeans(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=32)

    records: fields.ReverseRelation["OccurrenceRecord"]

    class Meta:
        table = "core_establishmentmeans"


class LifeStage(Model):
    id = fields.IntField(pk=True)
    short_name = fields.CharField(max_length=10)
    name = fields.CharField(max_length=32)

    records: fields.ReverseRelation["OccurrenceRecord"]

    class Meta:
        table = "core_lifestage"


class AdministrativeDivision(Model):
    id = fields.IntField(pk=True)
    country_code = fields.CharField(max_length=4)
    state_province = fields.CharField(max_length=64)
    country = fields.CharField(max_length=32)
    municipality = fields.CharField(max_length=32)

    locations: fields.ReverseRelation["Location"]

    class Meta:
        table = "core_administrativedivision"


class Location(Model):
    id = fields.IntField(pk=True)
    locality = fields.CharField(max_length=128)
    administrative_division = fields.ForeignKeyField(
        "models.AdministrativeDivision",
        related_name="locations",
        on_delete="RESTRICT",
        null=True
    )

    records: fields.ReverseRelation["OccurrenceRecord"]

    class Meta:
        table = "core_location"


class Journal(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=128)
    country = fields.CharField(max_length=32)

    references: fields.ReverseRelation["Reference"]

    class Meta:
        table = "core_journal"


class ReferenceType(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=32)

    references: fields.ReverseRelation["Reference"]

    class Meta:
        table = "core_referencetype"


class Reference(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=128, unique=True)
    cite = fields.CharField(max_length=1024)
    doi = fields.CharField(max_length=128)
    url = fields.CharField(max_length=512)
    journal = fields.ForeignKeyField(
        "models.Journal", related_name="references", on_delete="RESTRICT", null=True
    )
    reference_type = fields.ForeignKeyField(
        "models.ReferenceType",
        related_name="references",
        on_delete="RESTRICT",
        null=True,
    )
    authors = fields.ManyToManyField(
        "models.Author",
        related_name="references",
        through="core_reference_authors",
    )

    records: fields.ReverseRelation["OccurrenceRecord"]

    class Meta:
        table = "core_reference"


class Author(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=128)

    references: fields.ReverseRelation["Reference"]
    taxons: fields.ReverseRelation["Taxon"]

    class Meta:
        table = "core_author"


class OccurrenceRecord(Model):
    id = fields.IntField(pk=True)
    uuid = fields.UUIDField(unique=True, default=uuid4)
    quantity = fields.SmallIntField(null=True)
    date = fields.DateField(null=True)
    latitude = fields.FloatField(null=True)
    longitude = fields.FloatField(null=True)
    min_altitude = fields.FloatField(null=True)
    max_altitude = fields.FloatField(null=True)
    comments = fields.TextField(null=True)
    validated = fields.BooleanField(default=False)
    deleted = fields.BooleanField(default=False)
    data_provider = fields.ForeignKeyField(
        "models.DataProvider",
        related_name="records",
        null=True,
        on_delete="SET NULL",
    )
    establishment = fields.ForeignKeyField(
        "models.Establishmentmeans",
        related_name="records",
        null=True,
        on_delete="RESTRICT",
    )
    life_stage = fields.ForeignKeyField(
        "models.LifeStage",
        related_name="records",
        null=True,
        on_delete="RESTRICT",
    )
    location = fields.ForeignKeyField(
        "models.Location",
        related_name="records",
        null=True,
        on_delete="SET NULL",
    )
    record_type = fields.ForeignKeyField(
        "models.RecordType", related_name="records", on_delete="RESTRICT"
    )
    reference = fields.ForeignKeyField(
        "models.Reference",
        related_name="records",
        null=True,
        on_delete="RESTRICT",
    )
    substratum_type = fields.ForeignKeyField(
        "models.SubstratumType",
        related_name="records",
        null=True,
        on_delete="SET NULL",
    )
    taxon = fields.ForeignKeyField(
        "models.Taxon",
        related_name="records",
        on_delete="RESTRICT",
    )
    image = fields.ManyToManyField(
        "models.Image",
        related_name="records",
        through="core_occurrencerecord_images",
    )

    class Meta:
        table = "core_occurrencerecord"
