from typing import List

from fastapi import APIRouter
from tortoise.transactions import atomic

from app.modules.occurrence.models import (
    AdministrativeDivision,
    DataProvider,
    Establishmentmeans,
    Journal,
    LifeStage,
    Location,
    OccurrenceRecord,
    RecordType,
    Reference,
    ReferenceType,
    SubstratumType,
)
from app.modules.occurrence.schemas.occurrencerecord import (
    OccurrenceRecordCreateSchema,
    OccurrenceRecordPlainSchema,
    OccurrenceRecordSchema,
)
from app.modules.taxon.models import Taxon

router = APIRouter()


@router.get("/")
async def occurrence_quantity() -> int:
    quantity = await OccurrenceRecord.all()
    return len(quantity)


# uuid example: 3fa85f64-5717-4562-b3fc-2c963f66afa6
@router.get("/{occurrence_uuid}", response_model=OccurrenceRecordPlainSchema)
async def get_occurrence(occurrence_uuid: str) -> OccurrenceRecordPlainSchema:
    return await OccurrenceRecordPlainSchema.from_queryset_single(
        OccurrenceRecord.get(uuid=occurrence_uuid)
    )


@router.get("/taxon/", response_model=List[OccurrenceRecordPlainSchema])
async def get_occurrences(taxon_id: int) -> List[OccurrenceRecordPlainSchema]:
    return await OccurrenceRecordPlainSchema.from_queryset(
        OccurrenceRecord.filter(taxon_id=taxon_id)
    )


@atomic()
@router.post("/", response_model=OccurrenceRecordSchema)
async def create_occurrencerecord(
    occurrence: OccurrenceRecordCreateSchema,
) -> OccurrenceRecordSchema:
    record_type_db = await RecordType.get_or_create(name=occurrence.record_type)
    taxon_db = await Taxon.get(scientific_name=occurrence.taxon)

    occurrencerecord_db = await OccurrenceRecord.create(
        **occurrence.dict(
            exclude_unset=True,
            exclude={
                "taxon",
                "data_provider",
                "establishment",
                "life_stage",
                "location",
                "record_type",
                "reference",
                "substratum_type",
            },
        ),
        record_type_id=record_type_db[0].id,
        taxon_id=taxon_db.id
    )

    return await OccurrenceRecordSchema.from_queryset_single(
        OccurrenceRecord.get(id=occurrencerecord_db.id)
    )
