from fastapi import APIRouter

from app.modules.occurrence.api.endpoints import occurrence

api_router = APIRouter()

api_router.include_router(
    occurrence.router, prefix="/occurrences", tags=["occurrences"]
)
