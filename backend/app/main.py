import uvicorn
from fastapi import FastAPI

from app.core.config import settings
from app.db.base import init_db
from app.modules.occurrence.api.router import api_router as occurrence_router
from app.modules.taxon.api.router import api_router as taxon_router

app = FastAPI(title=settings.app_name)
app.include_router(taxon_router)
app.include_router(occurrence_router)


@app.on_event("startup")
async def startup_event() -> None:
    init_db(app)


if __name__ == "__main__":
    uvicorn.run("main:app", reload=True)
