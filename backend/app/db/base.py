from fastapi import FastAPI
from tortoise import Tortoise
from tortoise.contrib.fastapi import register_tortoise

from app.core.config import settings

MODELS = ["app.modules.occurrence.models", "app.modules.taxon.models"]


def init_db(app: FastAPI) -> None:
    register_tortoise(
        app,
        db_url=settings.database_uri,
        modules={"models": MODELS},
        generate_schemas=False,
        add_exception_handlers=True,
    )


def init_models() -> None:
    Tortoise.init_models(MODELS, "models")
