import os

from app.db.base import MODELS

TORTOISE_ORM = {
    "connections": {"default": os.environ.get("DATABASE_URI")},
    "apps": {
        "models": {
            "models": MODELS
            + [
                "aerich.models",
            ],
            "default_connection": "default",
        },
    },
}
