-- upgrade --
CREATE TABLE IF NOT EXISTS "core_administrativedivision" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "country_code" VARCHAR(4) NOT NULL,
    "state_province" VARCHAR(64) NOT NULL,
    "country" VARCHAR(32) NOT NULL,
    "municipality" VARCHAR(32) NOT NULL
);
CREATE TABLE IF NOT EXISTS "core_author" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(128) NOT NULL
);
CREATE TABLE IF NOT EXISTS "core_dataprovider" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(64) NOT NULL,
    "address" VARCHAR(64) NOT NULL,
    "city" VARCHAR(32) NOT NULL,
    "email" VARCHAR(32) NOT NULL,
    "telephone" VARCHAR(16) NOT NULL
);
CREATE TABLE IF NOT EXISTS "core_establishmentmeans" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(32) NOT NULL
);
CREATE TABLE IF NOT EXISTS "core_journal" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(128) NOT NULL,
    "country" VARCHAR(32) NOT NULL
);
CREATE TABLE IF NOT EXISTS "core_lifestage" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "short_name" VARCHAR(10) NOT NULL,
    "name" VARCHAR(32) NOT NULL
);
CREATE TABLE IF NOT EXISTS "core_location" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "locality" VARCHAR(128) NOT NULL,
    "administrative_division_id" INT NOT NULL REFERENCES "core_administrativedivision" ("id") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "core_recordtype" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(32) NOT NULL
);
CREATE TABLE IF NOT EXISTS "core_referencetype" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(32) NOT NULL
);
CREATE TABLE IF NOT EXISTS "core_reference" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(128) NOT NULL UNIQUE,
    "cite" VARCHAR(1024) NOT NULL,
    "doi" VARCHAR(128) NOT NULL,
    "url" VARCHAR(512) NOT NULL,
    "journal_id" INT NOT NULL REFERENCES "core_journal" ("id") ON DELETE RESTRICT,
    "reference_type_id" INT NOT NULL REFERENCES "core_referencetype" ("id") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "core_substratumtype" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL
);
CREATE TABLE IF NOT EXISTS "core_commonclassification" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "short_name" VARCHAR(3) NOT NULL UNIQUE,
    "name" VARCHAR(32) NOT NULL
);
CREATE INDEX IF NOT EXISTS "idx_core_common_short_n_b810d8" ON "core_commonclassification" ("short_name");
CREATE TABLE IF NOT EXISTS "core_image" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "image" VARCHAR(100) NOT NULL,
    "category" VARCHAR(64) NOT NULL,
    "description" VARCHAR(255) NOT NULL,
    "author" VARCHAR(64) NOT NULL,
    "source" VARCHAR(512) NOT NULL,
    "reputation" SMALLINT NOT NULL  DEFAULT 20
);
CREATE TABLE IF NOT EXISTS "core_kingdom" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(32) NOT NULL UNIQUE
);
CREATE TABLE IF NOT EXISTS "core_phylum" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(32) NOT NULL UNIQUE,
    "kingdom_id" INT NOT NULL REFERENCES "core_kingdom" ("id") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "core_class" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(32) NOT NULL UNIQUE,
    "phylum_id" INT NOT NULL REFERENCES "core_phylum" ("id") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "core_order" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(32) NOT NULL UNIQUE,
    "class_field_id" INT NOT NULL REFERENCES "core_class" ("id") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "core_family" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(32) NOT NULL,
    "order_id" INT NOT NULL REFERENCES "core_order" ("id") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "core_genus" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(32) NOT NULL UNIQUE,
    "family_id" INT NOT NULL REFERENCES "core_family" ("id") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "core_species" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(64) NOT NULL UNIQUE,
    "genus_id" INT NOT NULL REFERENCES "core_genus" ("id") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "core_subspecies" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(32) NOT NULL,
    "species_id" INT NOT NULL REFERENCES "core_species" ("id") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "core_taxon" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "scientific_name" VARCHAR(64) NOT NULL,
    "condensed_name" VARCHAR(64) NOT NULL UNIQUE,
    "author_id" INT REFERENCES "core_author" ("id") ON DELETE SET NULL,
    "common_classification_id" INT REFERENCES "core_commonclassification" ("id") ON DELETE RESTRICT,
    "species_id" INT NOT NULL REFERENCES "core_species" ("id") ON DELETE RESTRICT,
    "subspecies_id" INT REFERENCES "core_subspecies" ("id") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "core_occurrencerecord" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "uuid" UUID NOT NULL UNIQUE,
    "quantity" SMALLINT,
    "date" DATE,
    "latitude" DOUBLE PRECISION,
    "longitude" DOUBLE PRECISION,
    "min_altitude" DOUBLE PRECISION,
    "max_altitude" DOUBLE PRECISION,
    "comments" TEXT,
    "validated" BOOL NOT NULL  DEFAULT False,
    "deleted" BOOL NOT NULL  DEFAULT False,
    "data_provider_id" INT REFERENCES "core_dataprovider" ("id") ON DELETE SET NULL,
    "establishment_id" INT REFERENCES "core_establishmentmeans" ("id") ON DELETE RESTRICT,
    "life_stage_id" INT REFERENCES "core_lifestage" ("id") ON DELETE RESTRICT,
    "location_id" INT REFERENCES "core_location" ("id") ON DELETE SET NULL,
    "record_type_id" INT NOT NULL REFERENCES "core_recordtype" ("id") ON DELETE RESTRICT,
    "reference_id" INT REFERENCES "core_reference" ("id") ON DELETE RESTRICT,
    "substratum_type_id" INT REFERENCES "core_substratumtype" ("id") ON DELETE SET NULL,
    "taxon_id" INT NOT NULL REFERENCES "core_taxon" ("id") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "core_commonname" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(64) NOT NULL,
    "taxon_id" INT NOT NULL REFERENCES "core_taxon" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "core_synonym" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(128) NOT NULL,
    "taxon_id" INT NOT NULL REFERENCES "core_taxon" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "core_webresource" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(64) NOT NULL,
    "url" VARCHAR(200) NOT NULL,
    "taxon_id" INT NOT NULL REFERENCES "core_taxon" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(20) NOT NULL,
    "content" JSONB NOT NULL
);
CREATE TABLE IF NOT EXISTS "core_reference_authors" (
    "core_reference_id" INT NOT NULL REFERENCES "core_reference" ("id") ON DELETE CASCADE,
    "author_id" INT NOT NULL REFERENCES "core_author" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "core_taxon_images" (
    "core_taxon_id" INT NOT NULL REFERENCES "core_taxon" ("id") ON DELETE CASCADE,
    "image_id" INT NOT NULL REFERENCES "core_image" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "core_occurrencerecord_images" (
    "core_occurrencerecord_id" INT NOT NULL REFERENCES "core_occurrencerecord" ("id") ON DELETE CASCADE,
    "image_id" INT NOT NULL REFERENCES "core_image" ("id") ON DELETE CASCADE
);
