-- upgrade --
ALTER TABLE "core_location" ALTER COLUMN "administrative_division_id" DROP NOT NULL;
ALTER TABLE "core_reference" ALTER COLUMN "journal_id" DROP NOT NULL;
ALTER TABLE "core_reference" ALTER COLUMN "reference_type_id" DROP NOT NULL;
-- downgrade --
ALTER TABLE "core_location" ALTER COLUMN "administrative_division_id" SET NOT NULL;
ALTER TABLE "core_reference" ALTER COLUMN "journal_id" SET NOT NULL;
ALTER TABLE "core_reference" ALTER COLUMN "reference_type_id" SET NOT NULL;
